import React, {Component} from 'react';
import axios from 'axios';
import './ToDoList.css';
import Spinner from "../../components/Spinner/Spinner";

class ToDoList extends Component {
    state = {
        tasks: [],
        text: '',
        loading: false
    };

    newRequest = () => {
        axios.get('/tasks.json').then(response => {
            const tasks = [];
            for (let key in response.data) {
                tasks.push({...response.data[key], id: key});
            }
            this.setState({tasks, loading: false});
        });
    };

    componentDidMount() {
        this.setState({loading: true});
        this.newRequest();
    }

    handleChange = (event) => {
        this.setState({text: event.target.value});
    };

    handleClick = (e) => {
        e.preventDefault();
        if (this.state.text !== '') {
            this.setState({loading: true});
            axios.post('/tasks.json', {task: this.state.text}).then((response) => {
                const tasks = [...this.state.tasks];
                tasks.push({task: this.state.text, id: response.data.name});
                this.setState({tasks, loading: false});
            });
        } else {
            alert('Please add a task!');
        }
    };

    removeTask = (id) => {
        this.setState({loading: true});
        const index = this.state.tasks.findIndex(div => div.id === id);
        const tasks = [...this.state.tasks];
        axios.delete(`/tasks/${tasks[index].id}.json`).then(() => {
            this.newRequest();
        });
    };

    render() {
        let spinner;
        if (this.state.loading) {
            spinner = <Spinner />;
        }
        return (
            <div className="ToDoList">
                {spinner}
                <form>
                    <input placeholder="Your task ..." value={this.state.text} onChange={this.handleChange} id="task"
                           type="text"/>
                    <button onClick={this.handleClick} id="add"><i className="fa fa-arrow-circle-down"
                                                                   aria-hidden="true"/></button>
                </form>
                {this.state.tasks.map((task, k) => <div className="txt" key={k}>{task.task}<i
                    onClick={() => this.removeTask(task.id)} className="fa fa-trash-o" aria-hidden="true"/></div>)}
            </div>
        );
    }
}

export default ToDoList;