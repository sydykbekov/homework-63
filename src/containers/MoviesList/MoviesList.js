import React, {Component} from 'react';
import axios from 'axios';
import './MoviesList.css';
import Films from './components/Films/Films';
import Spinner from '../../components/Spinner/Spinner';

class MoviesList extends Component {
    state = {
        movies: [],
        movie: '',
        loading: false
    };

    newRequest = () => {
        axios.get('/movies.json').then(response => {
            const movies = [];
            for (let key in response.data) {
                movies.push({...response.data[key], id: key});
            }
            this.setState({movies, loading: false});
        });
    };

    componentDidMount() {
        this.setState({loading: true});
        this.newRequest();
    }

    handleChange = (event) => {
        this.setState({movie: event.target.value});
    };

    handleClick = (e) => {
        e.preventDefault();
        if (this.state.movie !== '') {
            this.setState({loading: true});
            axios.post('/movies.json', {movie: this.state.movie}).then((response) => {
                const movies = [...this.state.movies];
                movies.push({movie: this.state.movie, id: response.data.name});
                this.setState({movies, loading: false});
            });
        } else {
            alert('Please add a film!');
        }
    };

    changePost = (event, index) => {
        let filmIndex;
        const movies = [...this.state.movies];
        movies.forEach((item, i) => item.id === index ? filmIndex = i : null);
        const movie = {...this.state.movies[filmIndex]};
        movie.movie = event.target.value;
        movies[filmIndex] = movie;
        this.setState({movies});
        axios.patch(`/movies/${movie.id}.json`, {movie: movie.movie});
    };

    removeFilm = (id) => {
        this.setState({loading: true});
        const index = this.state.movies.findIndex(div => div.id === id);
        const movies = [...this.state.movies];
        axios.delete(`/movies/${movies[index].id}.json`).then(() => {
            this.newRequest();
        });
    };

    render() {
        let spinner;
        if (this.state.loading) {
            spinner = <Spinner />;
        }
        return (
            <div className="App">
                {spinner}
                <form>
                    <input placeholder="Film ..." value={this.state.movie} onChange={this.handleChange} id="task"
                           type="text"/>
                    <button onClick={this.handleClick} id="add">Add</button>
                </form>
                {this.state.movies.map((movie, k) =>
                    <Films key={k} change={(event) => this.changePost(event, movie.id)}
                           removeFilm={() => this.removeFilm(movie.id)} text={movie.movie}
                           blur={this.newRequest}
                    />
                )}
            </div>
        );
    }
}

export default MoviesList;