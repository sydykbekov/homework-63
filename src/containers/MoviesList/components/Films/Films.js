import React from 'react';

const Films = (props) => {
    return (
        <div id="container">
            <div className="txt">
                <input onBlur={props.blur} className="film" onChange={props.change} value={props.text}
                       type="text"/>
                <i className="fa fa-trash-o" aria-hidden="true" onClick={props.removeFilm}/>
            </div>
        </div>
    )
};

export default Films;