import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import HomePage from "./containers/HomePage/HomePage";
import ToDoList from "./containers/ToDoList/ToDoList";
import MoviesList from "./containers/MoviesList/MoviesList";

class App extends Component {
    render() {
        return (
            <Switch>
                <Route path="/" exact component={HomePage}/>
                <Route path="/ToDoList" component={ToDoList}/>
                <Route path="/MoviesList" component={MoviesList}/>
                <Route render={() => <h1>404 Not Found</h1>}/>
            </Switch>
        );
    }
}

export default App;
