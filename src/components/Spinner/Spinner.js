import React from 'react';
import './Spinner.css';

const Spinner = () => (
    <div className="SpinnerContainer">
        <div className="Spinner" />
    </div>

);

export default Spinner;
